# IoT Light Presence Detector

## Project Overview
IoT Light Presence Detector is a project designed to detect the presence of light and evaluate the intensity of the light. This project uses a *LDR (Light Dependent Resistor)* to evaluate the intensity of light and reports the data to the server over the internet.

Monitoring light intensity for indoor farming is a very important use case where this project can be very useful. You may also use this project for measuring Darkness or Relative Fluid Density.

If you want to operate your garden or street lights depending on the intensity of light present at any given point in time, you could do it by combining this project and the [IoT Remote Control](https://gitlab.com/micro-iot-platform/projects/iot-remote-control) project. This will reduce effort of switching on/off the lights and will also be a good way to save energy. 

## Project Details
This project helps us to evaluate the intensity of light and send the data over the internet. You can also see a report of varying light intensity. You can take necessary action depending on the reported data.

This is a project built using the [EmMate Framework](https://mig.iquesters.com/?s=embedded) of the [Micro IoT Platform](https://mig.iquesters.com). This project works seamlessly with [SoMThing](https://mig.iquesters.com/?s=somthing)s and connects to the [migCloud](https://mig.iquesters.com/?s=cloud) portal by default.
<br>[EmMate Framework](https://mig.iquesters.com/?s=embedded) is a multi-architecture, platform independent middleware of the [Micro IoT Platform](https://mig.iquesters.com), that can be used to build any micro to small scale embedded application.
<br>[SoMThing](https://mig.iquesters.com/?s=somthing) is a hardware ecosystem consisting of SoM (the processor), Thing (the application board) and Peripherals (sensors & actuators). 
<br>[migCloud](https://mig.iquesters.com/?s=cloud) is a IoT Web Server having a configurable utilities API gateway

## Hardware Requirement
You will need a micro controller based board which has a LDR interface. It should also have a minimum connectivity interface like WiFi. You can power up the board with a USB cable for development purposes, but it is recommended to power up the board using a dedicated power supply for trouble free operations.

*We will use the SoMThing ecosystem, viz, SoM - System on Module, Thing Boards and Peripherals in this project.*

You will need the following:
* SoM ESP32-WROOM-32D - 1 no.
* Team Thing Development board - 1 no.
* Light Detect Peripheral - 1 no.
* Micro-USB Cable  - 1 no.
* Adapter with a rating of 12V, 2A - 1 no.

If you do not have the required hardware, you may [explore available hardware](https://mig.iquesters.com/?s=somthing&p=resources) in the SoMThing ecosystem.

## Hardware Assembly
The hardware assembly is pretty simple. Follow the below image as a setup guide.

<img src="https://gitlab.com/micro-iot-platform/projects/light-presence-detector/raw/master/res/images/light_presence_detector.jpg" width="500">

Steps to follow:
* Take the Team Thing board
* Carefully connect the SoM in the TeamThing board
* Connect the Light Detect Peripheral board in the mikroBUS socket.

## Software Requirement
* This project is written in C programming language.
* The project is based on the [EmMate Framework](https://mig.iquesters.com/?s=embedded&p=documentation) and thus requires the framework to build it.
<br>You must [download the EmMate framework](https://mig.iquesters.com/?s=embedded&p=downloads) and follow the [EmMate Documentation](https://mig.iquesters.com/?s=embedded&p=readme&path=master/doc/getting-started/getting_started_with_emmate.md#gettingstartedwiththeemmateframework) to start using the framework.
* You must also [download the EmMate Configurer android app](https://mig.iquesters.com/apk/EmMate_Configurer.apk), which is used to configure the hardware.
* Use the migCloud portal to access and control you hardware from the internet.

## Setting up the Project
* To setup this project, refer the [Setting up EmMate Examples and Applications](https://mig.iquesters.com/?s=embedded&p=readme&path=master/doc/getting-started/getting_started_with_emmate.md#settingupemmateexamplesandapplications) section in the [EmMate Documentation](https://mig.iquesters.com/?s=embedded&p=readme&path=master/doc/getting-started/getting_started_with_emmate.md).

## Calculating the Light Intensity
* This project makes use of [this blog](https://www.allaboutcircuits.com/projects/design-a-luxmeter-using-a-light-dependent-resistor/) to make the necessary calculations
* You must take the necessary steps given in this blog, to characterize your LDR so that you will get an accurate result
* After characterizing your LDR, you need to plug in the values to the macros *LUX_CALC_SCALAR* and *LUX_CALC_EXPONENT* in the *calculate_light_intensity()* function, in the *light_presence_detector_app.c* file

## Related Documents
* SoM ESP32-WROOM-32D - [Datasheet](https://mig.iquesters.com/section/somthing/static/data-sheet/som/som-esp32-datasheet-v1.0.pdf)    [Schematic](https://mig.iquesters.com/section/somthing/static/schematic/som/som-esp32.pdf)
* TeamThing Development Board - [Datasheet](https://mig.iquesters.com/section/somthing/static/data-sheet/thing/team-thing-datasheet-v2.0.pdf)   [Schematic](https://mig.iquesters.com/section/somthing/static/schematic/thing/team-thing-v2.0.pdf)
* Light Detect Peripheral Board - [Datasheet](https://mig.iquesters.com/section/somthing/static/data-sheet/peripheral/light-detect-peripheral-datasheet.pdf)  [Schematic](https://mig.iquesters.com/section/somthing/static/schematic/peripheral/light-detect-peripheral.pdf)

## Know More
To know more about the:
* Micro IoT Platform [click here](https://mig.iquesters.com)
* SoMThing hardware ecosystem [click here](https://mig.iquesters.com/?s=somthing)
* EmMate development framework [click here](https://mig.iquesters.com/?s=embedded)
* migCloud portal [click here](https://mig.iquesters.com/?s=cloud)
