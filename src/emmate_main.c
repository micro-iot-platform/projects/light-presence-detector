
#include "light-presence-detector-app/light_presence_detector_app.h"

#define TAG	"emmate_main"

TaskRet emmate_main(void * param)
{
	EM_LOGI(TAG, "==================================================================");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "Starting application built on the Iquester Core Embedded Framework");
	EM_LOGI(TAG,"");
	EM_LOGI(TAG, "==================================================================");

	light_presence_detector_init();

	while(1){
		light_presence_detector_loop();
		TaskDelay(DELAY_5_SEC / TICK_RATE_TO_MS);
	}
}

