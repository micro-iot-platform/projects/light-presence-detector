/*
 * app_json_utils.c
 *
 *  Created on: 12-Aug-2019
 *      Author: debashis
 */

#include "app_json_utils.h"

#include "parson.h"
#include "core_utils.h"

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#include <string.h>
#include <stdlib.h>
#include <math.h>

#define TAG "app_json_utils"

em_err make_light_presence_publish_json(LIGHT_DETECTOR_PUBLISH_DATA *publish_data, char **ppbuf,
		size_t *plen) {
	em_err ret = EM_FAIL;

	char *serialized_string = NULL;

	if (NULL != publish_data) {
		// root obj
		JSON_Value *root_value = json_value_init_object();
		JSON_Object *root_object = json_value_get_object(root_value);

		json_object_set_number(root_object, GET_VAR_NAME(publish_data->light_intensity, "->"),
				publish_data->light_intensity);

//		serialized_string = json_serialize_to_string(root_value);
//		size_t len = json_serialization_size(root_value);

		serialized_string = json_serialize_to_string_pretty(root_value);
		size_t len = json_serialization_size_pretty(root_value);

		//printf("%s\r\n", serialized_string);

		len = len + 1;  // since json_serialization_size returns size + 1
		//printf("JSON Len = %d\r\n", len);

		char *ptemp = (char*) calloc(len, sizeof(char));
		if (ptemp == NULL) {
			EM_LOGE(TAG, "Failed to create application's Post Json for Low Memory");
			*ppbuf = NULL;
			*plen = 0;

			json_value_free(root_value);
			if (serialized_string != NULL) {
				json_free_serialized_string(serialized_string);
			}

			return EM_ERR_NO_MEM;
		}

		memset(ptemp, 0x00, len);
		memcpy(ptemp, serialized_string, len);
		*plen = len;
		*ppbuf = ptemp;

		json_value_free(root_value);
		if (serialized_string != NULL) {
			json_free_serialized_string(serialized_string);
		}
		ret = EM_OK;
	} else {
		ret = EM_ERR_INVALID_ARG;
	}
	return ret;

}

/*
 * Parse Application Config's response Json String:
 * {
 * 	"light_presence_publish_inteval" : 500
 * }
 *
 */
em_err parse_config_json(char *string, LIGHT_DETECTOR_CONFIG_DATA *resp) {
	em_err ret = EM_FAIL;

	if (NULL != string) {
		JSON_Value *root_value;
		JSON_Object *root_object;
		root_value = json_parse_string(string);

		if (json_value_get_type(root_value) != JSONObject) {
			EM_LOGE(TAG, "Config Respons JSON Value type not matched\r\n");
			return ret;
		}

		root_object = json_value_get_object(root_value);
		JSON_Value *keys;

//		keys = json_object_get_value(root_object, GET_VAR_NAME(resp->light_presence_read_interval, "->"));
//		if (json_value_get_type(keys) == JSONNumber) {
//
//			resp->light_presence_read_interval = json_object_get_number(root_object,
//					GET_VAR_NAME(resp->light_presence_read_interval, "->"));
//		}

		keys = json_object_get_value(root_object, GET_VAR_NAME(resp->light_presence_publish_interval, "->"));
		if (json_value_get_type(keys) == JSONNumber) {

			resp->light_presence_publish_interval = json_object_get_number(root_object,
					GET_VAR_NAME(resp->light_presence_publish_interval, "->"));
		}

		ret = EM_OK;
		json_value_free(root_value);

	} else {
		ret = EM_ERR_INVALID_RESPONSE;
	}
	return ret;

}
