/*
 * light_presence_detector_app.h
 *
 *  Created on: 10-Aug-2019
 *      Author: debashis
 */

#ifndef LIGHT_PRESENCE_DETECTOR_APP_H_
#define LIGHT_PRESENCE_DETECTOR_APP_H_

#include "emmate.h"
#include "thing.h"

typedef struct {
	uint32_t light_presence_publish_interval;
} LIGHT_DETECTOR_CONFIG_DATA;

typedef struct {
	int light_intensity;
} LIGHT_DETECTOR_PUBLISH_DATA;

typedef struct {
	LIGHT_DETECTOR_CONFIG_DATA app_config_data;
	LIGHT_DETECTOR_PUBLISH_DATA publish_data;
} LIGHT_DETECTOR_APP_DATA;


void light_presence_detector_init();
void light_presence_detector_loop();

#endif /* LIGHT_PRESENCE_DETECTOR_APP_H_ */
