@echo OFF

:: setting current directory
set CURR_DIR=%cd%
::echo %CURR_DIR%

:: importing Configuration File
set CONFIG_PATH=%CURR_DIR%\setup_conf.bat
echo %CONFIG_PATH%

::for /f "delims=" %%x in (%CONFIG_PATH%) do (set "%%x")
::echo {%RELEASE_PKG_DIR%}
::PAUSE

:: Source or Import the Config-file
call %CONFIG_PATH%
::echo %RELEASE_PKG_DIR%

set CURRENT_DATE=%date%
set CURRENT_TIME=%time%

::echo %CURRENT_DATE%
::echo %CURRENT_TIME%


echo .....................................................................
echo Setting up your project
echo .....................................................................
echo Core distribution Location : %RELEASE_PKG_DIR%
echo .....................................................................


IF "%RELEASE_PKG_DIR%"=="" (
	echo "Environment variable 'EMMATE_PATH' is not set"
	echo "You need to update your 'EMMATE_PATH' environment variable."
	PAUSE
	exit /b 1
)


set EMMATE_CORE_SRC=%RELEASE_PKG_DIR%\src
set EMMATE_PLATFORM_SRC=%RELEASE_PKG_DIR%\platform
set EMMATE_SIMULATOR_SRC=%RELEASE_PKG_DIR%\simulator


cd ..
set PROJ_DIR=%cd%
for /f "delims=" %%i in ("%PROJ_DIR%") do (
		set PROJ_NAME=%%~ni
)

::echo %PROJ_DIR%

echo Project Name : %PROJ_NAME%
echo Project Location : %PROJ_DIR%
echo .....................................................................

rem ############################################################### Project Clean ##############################################################################################
if "%1" == "-clean" (
	echo ----------------------------------- CLEAN %PROJ_DIR%\emmate DIRECTORY------------------------------------
	echo.
		rd /s /q %PROJ_DIR%\emmate
	
	echo ----------------------------------- CLEAN %PROJ_DIR%\build DIRECTORY------------------------------------
	echo.
		rd /s /q %PROJ_DIR%\build
	
	echo
	echo.
	echo ##### "%PROJ_NAME%" Project Setup Clean Complete ##############################################"
	
	cd %CURR_DIR%
	exit /b 0
)
rem #############################################################################################################################################################

rem copying a project template files
cd %RELEASE_PKG_DIR%\resource\files\emmate-sample-proj

echo.
echo ##### Copying Project's Build Files ##############################################
echo.

echo.
rem Copy the root CMAkeLists.txt file to the project root
echo ----- Copying CMakeLists.txt to %PROJ_DIR% -----------------------------
xcopy /I /D /Y /F .\CMakeLists.txt %PROJ_DIR%

echo.
rem Always copy the build folder from RELEASE_PKG_DIR/setup/files
if not exist %PROJ_DIR%\build\NUL mkdir %PROJ_DIR%\build
echo ----- Copying build [directory] to %PROJ_DIR% -----------------------------
xcopy /S /I /D /E /H /Y /F .\build\* %PROJ_DIR%\build

echo.
echo.
echo ##### Copying Project's Source Template / Eclipse setup Files ##############################################
echo.

IF NOT EXIST "%PROJ_DIR%\src" (
	echo.
	rem create "src" director at $PROJ_DIR
	mkdir %PROJ_DIR%\src
	
	echo.
	rem copy projects src templates files
	echo ----- Copying Project's src[directory] to %PROJ_DIR% -----------------------------
	xcopy /S /I /D /E /H /Y /F .\src\* %PROJ_DIR%\src
	
	echo.
	echo ----- Copying Project's Eclipse setup files to %PROJ_DIR% -----------------------------
	xcopy /S /I /D /E /H /Y /F %cd%\.externalToolBuilders\* %PROJ_DIR%\.externalToolBuilders
 	xcopy /S /I /D /E /H /Y /F %cd%\.launches-windows\* %PROJ_DIR%\.launches
rem 	xcopy /S /I /D /E /H /Y /F %cd%\.settings\* %PROJ_DIR%\.settings
 	xcopy /I /D /Y /F %cd%\.cproject %PROJ_DIR%
 	xcopy /I /D /Y /F %cd%\.project %PROJ_DIR%
	

) ELSE (
	echo.
	echo ----- Copying Project's Eclipse setup files to %PROJ_DIR% -----------------------------
	xcopy /S /I /D /E /H /Y /F %cd%\.externalToolBuilders\* %PROJ_DIR%\.externalToolBuilders
 	xcopy /S /I /D /E /H /Y /F %cd%\.launches-windows\* %PROJ_DIR%\.launches
rem 	xcopy /S /I /D /E /H /Y /F %cd%\.settings\* %PROJ_DIR%\.settings
 	xcopy /I /D /Y /F %cd%\.cproject %PROJ_DIR%
 	xcopy /I /D /Y /F %cd%\.project %PROJ_DIR%
)

rem ########################### Rename Eclipse Project Name ####################################################################
echo.
echo ----- Set the Eclipse Project name to %PROJ_NAME% -----------------------------
set "eclipse_projectfile=%PROJ_DIR%\.project"
set "search=emmate-sample-proj"
set "replace=%PROJ_NAME%"

set "inputFile=%eclipse_projectfile%"
echo "Input File = %inputFile%"
echo "New Name = %replace%"

for /f "delims=" %%i in ('type "%inputFile%" ^& break ^> "%inputFile%" ') do (
	set "line=%%i"
	setlocal enabledelayedexpansion
	>>"%inputFile%" echo(!line:%search%=%replace%!
	endlocal
)

echo.
echo.
echo ##### Copying Emmate's Source / Platform Files ##############################################
echo.

rem creating "emmate" directories at %PROJ_DIR%
if not exist %PROJ_DIR%\emmate\NUL mkdir %PROJ_DIR%\emmate

echo.
echo ----- Copying Emmate Source files to %PROJ_DIR% -----------------------------
cd %EMMATE_CORE_SRC%
xcopy /S /I /D /E /H /Y /F . %PROJ_DIR%\emmate
move %PROJ_DIR%\emmate\*.c	%PROJ_DIR%\src
rem rm -fv %PROJ_DIR%\emmate\*.c


if "%1" == "-simulator" (
	echo.
	echo ----- Copying Emmate Simulator files to %PROJ_DIR% -----------------------------
	cd %EMMATE_SIMULATOR_SRC%
	xcopy /S /I /E /H /Y /F . %PROJ_DIR%\emmate
	echo ----- Copying Emmate Simulator files to %PROJ_DIR% -----------------------------
	goto end
)

echo.
echo ----- Copying Emmate Platform files to %PROJ_DIR% -----------------------------
cd %EMMATE_PLATFORM_SRC%
rem copy files/folder with date modification use '/d'
rem xcopy /S /I /D /E /H /Y /F . %PROJ_DIR%\emmate		
xcopy /S /I /E /H /Y /F . %PROJ_DIR%\emmate
echo ----- Copying Emmate Platform files to %PROJ_DIR% -----------------------------


rem creating "emmate" resource directories at $PROJ_DIR
rem this will contain the platform's and simulator's source code
rem if not exist %PROJ_DIR%\emmate-resource\platform\NUL mkdir %PROJ_DIR%\emmate-resource\platform
rem if not exist %PROJ_DIR%\emmate-resource\simulator\NUL mkdir %PROJ_DIR%\emmate-resource\simulator
rem echo.
rem echo ----- Copying Emmate Resource files to $PROJ_DIR -----------------------------
rem cd %EMMATE_PLATFORM_SRC%
rem xcopy /S /I /D /E /H /Y /F . %PROJ_DIR%\emmate-resource\platform
rem cd %EMMATE_SIMULATOR_SRC%
rem xcopy /S /I /D /E /H /Y /F . %PROJ_DIR%\emmate-resource\simulator

:end
	echo.
	echo ##### "%PROJ_NAME%" Project Setup Complete ##############################################
	
	cd %CURR_DIR%
	exit /b 0