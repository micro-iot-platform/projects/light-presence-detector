#!/bin/bash

#setting current directory
export CURR_DIR=`pwd`

#importing Configuration File
export CONFIG_PATH=$CURR_DIR/setup.conf

source $CONFIG_PATH
export $(grep --regexp ^[A-Z] $CONFIG_PATH | cut -d= -f1)

export CURRENT_DATE=`date +%d-%b-%Y`
export CURRENT_TIME=`date +%H:%M:%S`

echo .....................................................................
echo Setting up your project
echo .....................................................................
echo Core distribution Location : $RELEASE_PKG_DIR
echo .....................................................................

if [ -z "$RELEASE_PKG_DIR" ]
then
	echo "Environment variable 'EMMATE_PATH' is not set"
	echo "You need to update your 'EMMATE_PATH' environment variable in the ~/.profile file."
	exit 1
fi


emmate_core_src=$RELEASE_PKG_DIR/src
emmate_platform_src=$RELEASE_PKG_DIR/platform
emmate_simulator_src=$RELEASE_PKG_DIR/simulator

cd ..
export PROJ_DIR=`pwd`
export PROJ_NAME=$(basename $PROJ_DIR)

echo Project Name : $PROJ_NAME
echo Project Location : $PROJ_DIR
echo .....................................................................

############################################################### Project Clean ##############################################################################################

if [[ $1 == "-clean" ]] 
then
	# ----------------------------------- CLEAN  $PROJ_DIR/emmate DIRECTORY------------------------------------
	echo ----------------------------------- CLEAN $PROJ_DIR/emmate DIRECTORY------------------------------------
	echo
		rm -rf -v $PROJ_DIR/emmate
	
	#echo ----------------------------------- CLEAN $PROJ_DIR/bin DIRECTORY------------------------------------
	#echo
		# .??* delete all hidden files
	#	rm -rf -v $PROJ_DIR/bin/.??*	$PROJ_DIR/bin/*
	#	rm -rf -v $PROJ_DIR/bin
	
	#echo ----------------------------------- CLEAN $PROJ_DIR/cmake_build DIRECTORY------------------------------------
	#echo
	#	rm -rf $PROJ_DIR/cmake_build/.??*	$PROJ_DIR/cmake_build/*
	#	rm -rf -v $PROJ_DIR/cmake_build
	# -------------------------------------------------------------------------------------------------
	echo
	echo "##### \"$PROJ_NAME\" Project Setup Clean Complete ##############################################"
	exit 0
fi

#############################################################################################################################################################





#copying a project template files
cd $RELEASE_PKG_DIR/resource/files/emmate-sample-proj

echo
echo "##### Copying Project's Build Files ##############################################"
echo

echo
# Copy the root CMAkeLists.txt file to the project root
echo "----- Copying CMakeLists.txt to $PROJ_DIR -----------------------------"
cp -v -u -r CMakeLists.txt $PROJ_DIR

# creating build directories
#mkdir -p $PROJ_DIR/build

echo
# Always copy the build folder from RELEASE_PKG_DIR/setup/files
echo "----- Copying build [directory] to $PROJ_DIR -----------------------------"
cp -v -u -r ./build $PROJ_DIR

echo
echo
echo "##### Copying Project's Source Template / Eclipse setup Files ##############################################"
echo

if [ ! -d "$PROJ_DIR/src" ]
then
	echo
	# create "src" director at $PROJ_DIR
	mkdir -p $PROJ_DIR/src
	
	echo
	#copy projects src templates files
	echo "----- Copying Project's src[directory] to $PROJ_DIR -----------------------------"
	cp -v -u -r ./src/ $PROJ_DIR
	
	echo
	echo "----- Copying Project's Eclipse setup files to $PROJ_DIR -----------------------------"
	cp -v -u -r ./.externalToolBuilders $PROJ_DIR
	cp -v -u -r ./.launches $PROJ_DIR
	cp -v -u -r ./.cproject $PROJ_DIR
	cp -v -u -r ./.project $PROJ_DIR

else
	echo
	echo "----- Copying Project's Eclipse setup files to $PROJ_DIR -----------------------------"
	cp -v -u -r ./.externalToolBuilders $PROJ_DIR
	cp -v -u -r ./.launches $PROJ_DIR
	cp -v -u -r ./.cproject $PROJ_DIR
	cp -v -u -r ./.project $PROJ_DIR

fi

########################### Rename Eclipse Project Name ###########################################
echo
echo "----- Set the Eclipse Project name to $PROJ_NAME -----------------------------"
export eclipse_projectfile=$PROJ_DIR/.project
export search=emmate-sample-proj
export replace=$PROJ_NAME

export inputFile=$eclipse_projectfile
echo "Input File = $inputFile"
echo "New Name = $replace"
sed -i "s/$search/$replace/g" "$inputFile"


echo
echo
echo "##### Copying Emmate's Source / Platform Files ##############################################"
echo

# creating "emmate" directories at $PROJ_DIR
mkdir -p $PROJ_DIR/emmate

echo
echo "----- Copying Emmate Source files to $PROJ_DIR -----------------------------"
cd $emmate_core_src
cp -v -u -r . $PROJ_DIR/emmate
mv -u -v -f $PROJ_DIR/emmate/*.c	$PROJ_DIR/src
rm -fv $PROJ_DIR/emmate/*.c


if [[ $1 == "-simulator" ]] 
then
	echo
	echo "----- Copying Emmate Simulator files to $PROJ_DIR -----------------------------"
	cd $emmate_simulator_src
	cp -v -r . $PROJ_DIR/emmate
	echo "----- Copying Emmate Simulator files to $PROJ_DIR -----------------------------"
else
	echo
	echo "----- Copying Emmate Platform files to $PROJ_DIR -----------------------------"
	echo
	cd $emmate_platform_src
	cp -v -r . $PROJ_DIR/emmate
	echo
	echo "----- Copying Emmate Platform files to $PROJ_DIR -----------------------------"
fi


# creating "emmate" resource directories at $PROJ_DIR
# this will contain the platform's and simulator's source code
# mkdir -p $PROJ_DIR/emmate-resource/platform
# mkdir -p $PROJ_DIR/emmate-resource/simulator
# echo
# echo "----- Copying Emmate Resource files to $PROJ_DIR -----------------------------"
# cd $emmate_platform_src
# cp -v -u -r . $PROJ_DIR/emmate-resource/platform
# cd $emmate_simulator_src
# cp -v -u -r . $PROJ_DIR/emmate-resource/simulator

cd $CURR_DIR

echo
echo "##### \"$PROJ_NAME\" Project Setup Complete ##############################################"
exit 0

