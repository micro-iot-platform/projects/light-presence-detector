#--------------------------------------------------------------------
# SETUP DIRECTORY CONTENTS
#--------------------------------------------------------------------
The setup directory must contain the following directories and files:

1. setup.bat							<FILE>
2. setup.sh								<FILE>
3. setup_conf.bat						<FILE>
4. setup.conf							<FILE>
5. readme.txt							<FILE>

1. setup.bat							<FILE>
The setup.bat file is the script (for Windows) using which 2 kinds of 
EmMate Project can be setup - Empty Project or Existing EmMate Project
Details are provide below


2. setup.sh								<FILE>
The setup.sh file is the script (for Ubuntu) using which 2 kinds of 
EmMate Project can be setup - Empty Project or Existing EmMate Project
Details are provide below


3. setup_conf.bat						<FILE>
The setup_conf.bat file is a configuration file for the script setup.bat
This file is for EmMate Framework Developers and should not be touched by the user


4. setup.conf							<FILE>
The setup.conf file is a configuration file for the script setup.sh
This file is for EmMate Framework Developers and should not be touched by the user


5. readme.txt							<FILE>
The readme.txt file is the file you are reading now.


#-------------------------------------------------------------------------------
# HOW TO SETUP AN EMMATE PROJECT
#-------------------------------------------------------------------------------
The below document assumes you have completed the EmMate Installation Process.
If not, read the getting started documentation.

There are 2 types of EmMate Projects which can be setup:
1. Empty Project
2. Existing EmMate Project

For both types of project, the setup utility must be run.

1. Empty Project
#-------------------------------------------------------------------------------
To create an empty project first create a new directory anywhere in your system.
For Windows users, create the diretory in the same drive where your 
EmMate Release Package resides. The directory name must not contain any spaces

The name you provide to this new directory becomes the name of your project

Inside this project directory copy the directory called 'setup' from 
'EmMate Release Package/resource'

Now open a command prompt / terminal inside the 'setup' directory and run the setup utility 
For Windows - setup.bat
For Ubuntu - ./setup.sh

Once the setup utility is run, it will copy all necessary files from the EmMate Release Package
into your project's directory. It will also setup an Eclipse Project from where you can
configure, build, flash and monitor your project


2. Existing EmMate Project
#-------------------------------------------------------------------------------
If you want to test the readily available EmMate Examples or Projects you will 
also need to run the setup utility

First, copy any example/project anywhere in your system. Please make sure you copy
it outside the EmMate Release Package.

For Windows users, copy the example/project in the same drive where your 
EmMate Release Package resides.

The examples/projects will already have a setup directory with required files. 
If it doesn't you can create it by following the steps provided above (1. Empty Project)

Now, go into the 'setup' directory and run the setup utility on a a command prompt / terminal
For Windows - setup.bat
For Ubuntu - ./setup.sh

Once the setup utility is run, it will copy all necessary files from the EmMate Release Package
into your project's directory. It will also setup an Eclipse Project from where you can
configure, build, flash and monitor your project
